Name: Steve Martin D. San Jose

Instruction to run

1.  install expo / expocli
2.  clone the project https://gitlab.com/stevemartin.2k16/news_hub_emapta.git
3.  install the dependecy type "npm install"
4.  run the project type "npm run start"
5.  it will popup the developer tool. which located in bottom-left corner. and now you can select whatever testing tool you want to use. it will log the instruction in case it will fail to run.

Other packages have been use

- Expo / Expo-ClI
  Development platform the same as react-native-cli, I use to this because the testing for emulator and simulator is more easy and the configuration and setup also is not more complicated than react-native-cli
- axios
  I use this for HTTPRequest to be able us to fetch the NewsAPI
- moment
  I use this to fix the date fron article into human readable format

import React, { Fragment, useState, useEffect} from 'react';
import { ScrollView, RefreshControl} from 'react-native';
import Article from './Article';
import axios from 'axios';

const ArticleLIst = (props) => {
  const [articles, setArticles] = useState([]);
  const [isRefresh, setRefresh] = useState(false);

  const fetchData = async () => {
    await axios.get('http://newsapi.org/v2/top-headlines?country=us&apiKey=0a9e7ea664474841ad56a70679cf643c').then((newsData) => {
      setArticles(newsData.data.articles);
      setRefresh(false);
    })
  }

  useEffect(() => {
     fetchData();
  }, [])

  refreshHandler = () => {
    setRefresh(true);
    fetchData();
  }

  return (
    <Fragment>
      <ScrollView refreshControl={
        <RefreshControl 
          refreshing={isRefresh}
          onRefresh={() => refreshHandler()}
        />
        }
      >
        {(articles && articles.length) >= 1 && (
          articles.map((article, index) => (<Article article={article} key={index}/>))
        )}
      </ScrollView>
    </Fragment>
  );
};

export default ArticleLIst;

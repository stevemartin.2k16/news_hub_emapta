import React from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity } from 'react-native';
import moment from 'moment';
import { useNavigation } from '@react-navigation/native';

const Article = (props) => {
  const navigation = useNavigation();
  return (
    <View style={style.article}>
      <TouchableOpacity onPress={() => navigation.navigate('Article', props.article)}>
        <Image 
          style={style.image} 
          source={{uri: props.article.urlToImage}}
        />
        <Text style={style.title}>{props.article.title}</Text>
        <Text>By {props.article.author}</Text>
        <Text style={style.description}>{props.article.description}</Text>
        <Text style={style.published}>Published {moment(props.article.publishedAt).format("MMM DD, YYYY")}</Text>
      </TouchableOpacity>
    </View>
  )
}

export default Article;

const style = StyleSheet.create({
  article: {
    backgroundColor: 'white',
    marginTop: 5,
    paddingBottom: 10
  },
  image: {
    width: '100%',
    height: 200,
  },
  title: {
    fontWeight: 'bold',
    color: '#3696d6'
  },
  description: {
    color: '#414242',
    fontWeight: '200',
    fontSize: 13,
    marginTop: 5
  },
  published: {
    color: 'grey',
    marginTop: 5,
    fontWeight: "600"
  }
})
import React, { useEffect, useState } from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity, Linking } from 'react-native';
import moment from 'moment';

const ArticleDetails = (props) => {
  return (
    <View style={style.ArticleDetails}>
      <Image style={style.image} source={{ uri: props.route.params.urlToImage }} />
      <TouchableOpacity onPress={() => { Linking.openURL(props.route.params.url)}}>
        <Text style={style.title}>{props.route.params.title}</Text>
      </TouchableOpacity>
      <Text>By {props.route.params.author}</Text>
      <Text style={style.published}>
        Published {moment(props.route.params.publishedAt).format('MMM DD, YYYY')}
      </Text>
      <Text style={style.content}>
        {props.route.params.content}
      </Text>
    </View>
  );
};

export default ArticleDetails;

const style = StyleSheet.create({
  ArticleDetails: {
    backgroundColor: 'white',
    paddingLeft: 5,
    paddingRight: 5,
    height: '100%',
  },
  image: {
    width: '100%',
    height: 200,
  },
  title: {
    fontWeight: 'bold',
    color: '#3696d6'
  },
  description: {
    color: '#414242',
    fontWeight: '200',
    fontSize: 13,
  },
  published: {
    color: 'grey',
    marginTop: 5,
    fontWeight: "600"
  },
  content: {
    justifyContent: "center",
    lineHeight: 20,
    marginTop: 5
  }
});

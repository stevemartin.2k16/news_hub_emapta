import React, { useEffect, useState} from 'react';
import { Text, View, StyleSheet} from 'react-native';
import ArticleList from './ArticleList';

const Home = () => {
 return (
  <View style={style.home}>
    <ArticleList/>
  </View>
 )
}

export default Home;

const style = StyleSheet.create({
  home: {
    backgroundColor: '#f2f2f0',
    paddingLeft: 5,
    paddingRight: 5,
  }
})

